package org.sikuli.api;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
/**
 * The API class allows the application to launch the default browser and pause.
 * 
 * @author Tom Yeh (tom.yeh@colorado.edu)
 *
 */
public class API {

	/**
	 * Causes the current thread to suspend execution for a specified number of milliseconds.
	 * 
	 * @param mills the length of time to pause in milliseconds
	 */
	static public void pause(int mills){		
		try {
			Thread.sleep(mills);
		} catch (InterruptedException e) {
		}		
	}
	
	/**
	 * Launches the default browser to open a URL.
	 * 
	 * @param url the URL to be opened in the user default browser
	 */
	static public void browse(URL url) {
		try {
			Desktop.getDesktop().browse(new URI(url.toString()));
		} catch (URISyntaxException e) {
		} catch (IOException e) {
		}
	}
	
}
