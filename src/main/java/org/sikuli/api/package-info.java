/**
* Provides the classes necessary to specify and locate targets on the screen.
*/

package org.sikuli.api;
