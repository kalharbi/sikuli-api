package org.sikuli.api.visual;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.net.URL;

import org.sikuli.api.ScreenLocation;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.DesktopScreen;

/**
 * A ScreenPainter for painting on screen.
 * 
 * @author Tom Yeh (tom.yeh@colorado.edu)
 *
 */
public class ScreenPainter {

	/**
	 * Displays box around the specified ScreenRegion. 
	 * @param r the ScreenRegion of the box
	 * @param duration how long to display the box in milliseconds
	 */	
	public void box(ScreenRegion r, int duration){
		Rectangle b = ((DesktopScreen) r.getScreen()).getBounds();
		new BoxOverlay(b.x+r.x, b.y+r.y, r.width, r.height).show(duration);
	}
	
	/**
	 * Displays circle around the specified ScreenRegion. 
	 * 
	 * @param r the ScreenRegion of the circle
	 * @param duration how long to display the circle in milliseconds
	 */
	public void circle(ScreenRegion r, int duration){
		Rectangle b = ((DesktopScreen) r.getScreen()).getBounds();
		new CircleOverlay(b.x+r.x, b.y+r.y, r.width, r.height).show(duration);
	}
	/**
	 * Displays circle around the specified ScreenLocation. 
	 * 
	 * @param l the ScreenLocation of the circle
	 * @param duration how long to display the circle in milliseconds
	 */
	public void circle(ScreenLocation l, int duration){
		Rectangle b = ((DesktopScreen) l.getScreen()).getBounds();
		new CircleOverlay(b.x + l.x-10, b.y + l.y-10, 20, 20).show(duration);
	}
	/**
	 * Displays a text label around the specified ScreenRegion.
	 * 	
	 * @param r the ScreenRegion of the label
	 * @param txt the text to be displayed
	 * @param duration how long to display the label in milliseconds
	 */	
	public void label(ScreenRegion r, String txt, int duration){
		label(r.getTopLeft().getAbove(5).getLeft(5), txt, duration);
	}
	/**
	 * Displays a text label around the specified ScreenLocation.
	 * 	
	 * @param l the ScreenLocation of the label
	 * @param txt the text to be displayed
	 * @param duration how long to display the label in milliseconds
	 */
	public void label(ScreenLocation l, String txt, int duration){
		Rectangle b = ((DesktopScreen) l.getScreen()).getBounds();
		new LabelOverlay(txt, b.x + l.x, b.y + l.y).show(duration);
	}
	/**
	 * Displays an image around the specified ScreenLocation.
	 * 
	 * @param l the ScreenLocation of the image
	 * @param image the image to be displayed 
	 * @param time how long to display the image in milliseconds
	 */
	public void image(ScreenLocation l, BufferedImage image, int time){
		Rectangle b = ((DesktopScreen) l.getScreen()).getBounds();
		new ImageOverlay(image, b.x + l.x, b.y + l.y).show(time);
	}		
}

